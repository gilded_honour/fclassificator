import h5py
import csv
import pandas

file_name = 'familyLabels.hdf'
file_name2 = 'predictions.hdf'
file_name3 = 'approved.hdf'
file_name4 = 'familyCorrections.hdf'

store = pandas.HDFStore(file_name)
store2 = pandas.HDFStore(file_name2)
store3 = pandas.HDFStore(file_name3)
store4 = pandas.HDFStore(file_name4)

# print '----------------------Original----------------------'
# print store['familyLabels'] # font-families : typekit-classes + 2 binary
# print store2['predictions'] # font-faces : typekit-classes + 2 binary (10)
# print store3['approved'] # font-families : typekit-classes + 2 binary

print '\n\n\n----------------------Corrections----------------------'
print store4['familyCorrections'] # font-families : typekit-classes (8)