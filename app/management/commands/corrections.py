from django.core.management.base import BaseCommand, CommandError
import pandas as pd
import os

from app import models

class Command(BaseCommand):
    help = ''
    can_import_settings = True
    args = '<out_folder out_folder>'

    def handle(self, *args, **options):
        full_file_name = os.path.join(args[1], 'familyCorrections_ui.hdf') if len(args) == 2 else 'familyCorrections_ui.hdf'
        # fc_list = models.FontClass.objects.all().filter(is_original=False)
        fc_list = models.FontClass.objects.all().filter(is_original=True)
        data = {}
        for fc_list_item in fc_list:
          res1 = {}
          for ff_list_item in fc_list_item.fontfamily_set.all():
              val = ff_list_item.fontclassvalue_set.all().filter(font_class=fc_list_item, font_family=ff_list_item).first().value
              res1[str(ff_list_item.name)] = val

          data[fc_list_item.name] = res1

        df = pd.DataFrame(data, columns=map(lambda x: str(x.name), fc_list)).fillna(0)
        df.to_hdf(full_file_name, 'familyCorrections')