from django.core.management.base import BaseCommand, CommandError
import pandas as pd
import os
from app import models

class Command(BaseCommand):
    help = ''
    can_import_settings = True
    args = '<out_folder out_folder>'

    def handle(self, *args, **options):
        if len(args) == 2:
            full_file_name = os.path.join(args[1], 'approved_ui.hdf')
        else:
           full_file_name = 'approved_ui.hdf'

        fc_list = models.FontClass.objects.all().filter(is_original=False)
        data = {}
        for fc_list_item in fc_list:
          res1 = {}
          for ff_list_item in fc_list_item.fontfamily_set.all():
              res1[str(ff_list_item.name)] = int(ff_list_item.is_approved)

          data[fc_list_item.name] = res1

        df = pd.DataFrame(data, columns=map(lambda x: str(x.name), fc_list))
        df.to_hdf(full_file_name, 'approved')