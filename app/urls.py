from django.conf.urls import patterns, url
from django.conf import settings
from app import views

urlpatterns = patterns('',
    url(r'^index$', views.index, name='home'),
    url(r'^approve_single$', views.approve_single, name='ffamily_approve_single'),
    url(r'^approve_all$', views.approve_all, name='ffamily_approve_all'),
    url(r'^unapprove_all$', views.unapprove_all, name='ffamily_disapprove_all'),
    url(r'^font_correction$', views.font_correction, name='font_correction'),
    url(r'^font_non_tk_correction$', views.font_non_tk_correction, name='font_non_tk_correction'),
)

if not settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )