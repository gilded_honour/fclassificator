from django.db import models
from django.db.models import Max
from django.db.models import Q

# the 8 special typekit classes + other classes
class FontClass(models.Model):
    name = models.CharField(max_length=64)
    is_original = models.BooleanField() 

    def is_typekit_class(self):
        return self.name.istartswith('tk_')

    def __str__(self):
        return "{}, is_original: {}".format(self.name, self.is_original)

class FontFamily(models.Model):
    prediction_threshold = 0.5

    name = models.CharField(max_length=64)
    url = models.URLField(null=True)
    is_approved = models.BooleanField(default=False)
    is_typekit_family = models.BooleanField() # does it come from typekit.com?
    is_whitelisted = models.BooleanField(default=False)
    font_classes = models.ManyToManyField(FontClass) # the 8 special typekit classes + other classes
    
    def is_font_class_predicted(self, font_class=None):
        pr_val = self.font_class_predicted_value(font_class)
        return pr_val > self.prediction_threshold
   
    def font_class_predicted_value(self, font_class_name=None):
        fpred = FontPrediction.objects.filter(font_face__font_family=self)
        if font_class_name != None:
            fpred = fpred.filter(font_class__name=font_class_name)
        
        return fpred.aggregate(Max('value'))['value__max']

    def font_typekit_class(self):
        return self.font_classes.all().filter(is_original=True, name__startswith='tk_', fontclassvalue__value=1, fontclassvalue__font_family=self).first()

    def font_other_classes(self):
        fc_list = self.font_classes.all().filter(is_original=True, fontclassvalue__value=1, fontclassvalue__font_family=self)
        return filter(lambda x: not x.name.startswith('tk_'), fc_list)

    def font_class_correction(self):
        return self.font_classes.all().filter(is_original=False, fontclassvalue__value=1, fontclassvalue__font_family=self).first()

    def set_new_tk_font_class_correction(self, new_font_class_id):
        # reset all values to 0
        fcv_list = FontClassValue.objects.all().filter(font_class__name__startswith='tk_', font_class__is_original=False, font_family=self)
        if fcv_list:
            fcv_list.update(value=0)

        # set the new value to 1
        if int(new_font_class_id) != 0:
            new_font_class = FontClass.objects.get(id=new_font_class_id)
            fcv = FontClassValue.objects.all().filter(font_class=new_font_class, font_family=self).first()
            if fcv:
                fcv.value = 1
            else:
                fcv = FontClassValue(font_class=new_font_class, font_family=self, value=1)
                self.font_classes.add(new_font_class)
                self.save()   
            
            fcv.save()
        else:
            raise ValueError('new_font_class_id has to have the value > 0')

    def get_font_class_non_tk_correction_status(self, non_tk_class):
        if not non_tk_class.name.startswith('b_'):
            raise ValueError('The name of non_tk_class_name has to start with b_')

        fc = FontClass.objects.get(name=non_tk_class.name)
        fcv = FontClassValue.objects.all().filter(font_class__is_original=True, font_class=fc, font_family=self).first()
        # fcv = FontClassValue.objects.all().filter(font_class=fc, font_family=self).first()
        return -1 if fcv is None or fcv.value is None else fcv.value

    def set_non_tk_font_class_correction(self, font_class_name, value):
        fc = FontClass.objects.get(name=font_class_name)
        fcv = FontClassValue.objects.all().filter(font_class=fc, font_family=self).first()
        if fcv:
            fcv.value = value
        else:
            fcv = FontClassValue(font_class=fc, font_family=self, value=value)
            self.font_classes.add(fc)
            self.save()   
            
        fcv.save()

#    def is_whitelisted(self):
#        return FontFamily.objects.filter(font_family=self).first()

    def __str__(self):
        return self.name

class FontClassValue(models.Model):
    value = models.IntegerField(default=None, null=True, blank=True)
    font_class = models.ForeignKey(FontClass)
    font_family = models.ForeignKey(FontFamily)
    
    def __str__(self):
        return "{}, {}, ({}, {})".format(self.id, self.value, self.font_class.name, self.font_family.name)

class FontFace(models.Model):
    name = models.CharField(max_length=64)
    font_family = models.ForeignKey(FontFamily)
    is_sortable = models.BooleanField(default=False, blank=True) 

    def __str__(self):
        return self.name

class FontPrediction(models.Model):
    font_face = models.ForeignKey(FontFace) #todo OneToOneField(FontFace)
    font_class = models.ForeignKey(FontClass)
    value = models.FloatField()

    def __str__(self):
        return str(self.value)



# import pdb; pdb.set_trace()
