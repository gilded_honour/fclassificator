import h5py
import csv
import pandas

file_name = 'familyLabels.hdf'
file_name2 = 'predictions.hdf'
file_name3 = 'approved.hdf'
file_name4 = 'familyCorrections.hdf'

store = pandas.HDFStore(file_name)
store2 = pandas.HDFStore(file_name2)
store3 = pandas.HDFStore(file_name3)
store4 = pandas.HDFStore(file_name4)

# print store['familyLabels']
# print store['familyLabels'].columns.values
print store2['predictions']
# print store3['approved']
# print store4['familyCorrections']