$(function() {

  //todo - refactor
  $('#btn_approve_all').click(function(e) {
    $.ajax({
      method: 'POST',
      url: 'approve_all'
      }).done(function(data) {
      // debugger
      alert('Ok approve_all');
    }).fail(function(xhr, errmsg, err) {
      alert('error');
    });
  }); 

  $('#btn_unapprove_all').click(function(e) {
    $.ajax({
      method: 'POST',
      url: 'unapprove_all'
    }).done(function(data) {
      // debugger
      alert('Ok unapprove_all');
    }).fail(function(xhr, errmsg, err) {
      alert('error');
    });
  }); 

  $('#font_families').on('click', '.btn_approve_font_family', function(e) {
    $.ajax({
      method: 'POST',
      url: 'approve_single',
      data: {'ffamily_id': $(this).data('ffamilyId')}
    }).done(function(data) {
      // debugger
      alert('Ok approve_single');
    }).fail(function(xhr, errmsg, err) {
      alert('error');
    });
  });

  $('#chk_positive').click(function(e) {
    // debugger

    
    if (this.checked) {
      if (window.location.href.search('[?&]') === -1) {
        window.history.pushState('object or string', 'Title', window.location.href + '?positive=true');  
      } else {
        window.history.pushState('object or string', 'Title', window.location.href.replace(/positive=(false|true)/i, 'positive=true'));   
      }

    } else {
      if (window.location.href.search('[?&]') === -1) {
        window.history.pushState('object or string', 'Title', window.location.href + '?positive=false');  
      } else {
        window.history.pushState('object or string', 'Title', window.location.href.replace(/positive=(false|true)/i, 'positive=false'));   
      }
    }
  }); 

});